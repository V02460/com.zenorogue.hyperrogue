# HyperRogue:

This is a flatpak (WIP) for [Hyperrogue](https://github.com/zenorogue/hyperrogue)

## How to test:

### Clone the repository:
    git clone https://gitlab.com/Rampoina/com.zenorogue.hyperrogue.git

### Initialize the submodule
    git submodule init && git submodule update

### Build the flatpak:
    flatpak-builder --force-clean build-dir com.zenorogue.hyperrogue.json

### Run the flatpak:
    flatpak-builder --run build-dir com.zenorogue.hyperrogue.json hyperrogue